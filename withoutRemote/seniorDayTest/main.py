import time
import serial
import RPi.GPIO as GPIO

GPIO.setwarnings(False)

# referring to the pins by GPIO numbers
GPIO.setmode(GPIO.BCM)

# define pi GPIO
GPIO_FRONT_TRIGGER = 23
GPIO_FRONT_ECHO    = 24
# GPIO_LEFT_TRIGGER = 27
# GPIO_LEFT_ECHO    = 22

# output pin: Trigger
GPIO.setup(GPIO_FRONT_TRIGGER,GPIO.OUT)
# GPIO.setup(GPIO_LEFT_TRIGGER,GPIO.OUT)
# input pin: Echo
GPIO.setup(GPIO_FRONT_ECHO,GPIO.IN)
# GPIO.setup(GPIO_LEFT_ECHO,GPIO.IN)
# initialize trigger pin to low
GPIO.output(GPIO_FRONT_TRIGGER, False)
# GPIO.output(GPIO_LEFT_TRIGGER, False)

serial_port = serial.Serial('/dev/ttyACM0', 115200, timeout=1)


# measure front distance
def measureFront():

  GPIO.output(GPIO_FRONT_TRIGGER, True)
  time.sleep(0.00001)
  GPIO.output(GPIO_FRONT_TRIGGER, False)
  frontStart = time.time()

  while GPIO.input(GPIO_FRONT_ECHO)==0:
    frontStart = time.time()

  while GPIO.input(GPIO_FRONT_ECHO)==1:
    frontStop = time.time()

  frontElapsed = frontStop-frontStart
  frontDistance = (frontElapsed * 34300)/2

  return frontDistance

'''
# measure left distance
def measureLeft():
  
  GPIO.output(GPIO_LEFT_TRIGGER, True)
  time.sleep(0.00001)
  GPIO.output(GPIO_LEFT_TRIGGER, False)
  leftStart = time.time()

  while GPIO.input(GPIO_LEFT_ECHO)==0:
    leftStart = time.time()

  while GPIO.input(GPIO_LEFT_ECHO)==1:
    leftStop = time.time()

  leftElapsed = leftStop-leftStart
  leftDistance = (leftElapsed * 34300)/2

  return leftDistance
'''

try:
    while True:
        '''
        # case for forward and overtaking with no other car in the overtaking lane
        frontDistance = int(measureFront())
        print("Front Distance : %.1f cm" % frontDistance)
        if frontDistance <= 40:
            print("Overtake Start")
            serial_port.write((chr(7)).encode())
            time.sleep(0.8) # sleep how long does it take to go in the new lane
            # while distance is less than a value that shows the car is beside ours
            while leftDistance <= 10:
               serial_port.write((chr(1)).encode())
            serial_port.write((chr(6)).encode())
            time.sleep(0.8) # sleep how long does it take to go back into original lane
            serial_port.write((chr(1)).encode()) #continue driving
            print("Overtake Stop")
        else:
            serial_port.write((chr(1)).encode())
            print("Forward")
        '''
    
        '''
        # hard code for overtake with only frontDistance
        frontDistance = int(measureFront())
        print("Front Distance : %.1f cm" % frontDistance)
        if frontDistance <= 50:
            print("Overtake Start")
            serial_port.write((chr(8)).encode())
            time.sleep(1) # sleep how long does it take to go in the new lane
            serial_port.write((chr(7)).encode())
            time.sleep(1.5) # sleep how long does it take to go in the new lane
            serial_port.write((chr(1)).encode())
            time.sleep(2) # sleep how long does it takes to pass the car
            serial_port.write((chr(6)).encode())
            time.sleep(3) # sleep how long does it take to go back into original lane
            serial_port.write((chr(1)).encode()) #continue driving
            print("Overtake Stop")
        else:
            serial_port.write((chr(1)).encode())
            print("Forward")
        
        '''
        # code for forward and reverse til stop
        frontDistance = int(measureFront())
        print("Front Distance : %.1f cm" % frontDistance)
        if frontDistance <= 50:
            serial_port.write((chr(2)).encode())
            print("Reverse")
            time.sleep(0.75)
            serial_port.write((chr(0)).encode())
            print("Stop")
        else:
            serial_port.write((chr(1)).encode())
            print("Forward")
        
        
        # send data to arduino every 0.5 sec
        time.sleep(0.5)
finally:
    GPIO.cleanup()
