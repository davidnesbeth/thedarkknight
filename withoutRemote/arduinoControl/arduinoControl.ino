// connect motor controller pins to Arduino digital pins
// back motor
int forwardEnable = 9;
int reverseEnable = 8;
// front motor
int rightEnable = 7;
int leftEnable = 6;

// delare variables for output duration
int time = 50;
int direction = 0;

void setup()
{
  // set all the motor control pins to outputs
  pinMode(forwardEnable, OUTPUT);
  pinMode(reverseEnable, OUTPUT);
  pinMode(rightEnable, OUTPUT);
  pinMode(leftEnable, OUTPUT);
  Serial.begin(115200);
}

void off(){
  digitalWrite(forwardEnable, LOW);
  digitalWrite(reverseEnable, LOW);
  digitalWrite(leftEnable, LOW);
  digitalWrite(rightEnable, LOW);
}

void forward(int time){
  digitalWrite(forwardEnable, HIGH);
  digitalWrite(reverseEnable, LOW);
  delay(time);
}

void reverse(int time){
  digitalWrite(forwardEnable, LOW);
  digitalWrite(reverseEnable, HIGH);
  delay(time);
}

void left(int time){
  digitalWrite(leftEnable, HIGH);
  digitalWrite(rightEnable, LOW);
  delay(time);
}

void right(int time){
  digitalWrite(leftEnable, LOW);
  digitalWrite(rightEnable, HIGH);
  delay(time);
}

void leftForward(int time){
  digitalWrite(forwardEnable, HIGH);
  digitalWrite(reverseEnable, LOW);
  digitalWrite(leftEnable, HIGH);
  digitalWrite(rightEnable, LOW);
  delay(time);
}

void rightForward(int time){
  digitalWrite(forwardEnable, HIGH);
  digitalWrite(reverseEnable, LOW);
  digitalWrite(leftEnable, LOW);
  digitalWrite(rightEnable, HIGH);
  delay(time);
}

void leftReverse(int time){
  digitalWrite(forwardEnable, LOW);
  digitalWrite(reverseEnable, HIGH);
  digitalWrite(leftEnable, HIGH);
  digitalWrite(rightEnable, LOW);
  delay(time);
}

void rightReverse(int time){
  digitalWrite(forwardEnable, LOW);
  digitalWrite(reverseEnable, HIGH);
  digitalWrite(leftEnable, LOW);
  digitalWrite(rightEnable, HIGH);
  delay(time);
}

void loop() {
  if (Serial.available() > 0){
    direction = Serial.read(); // read command
  }
  else{
    off();
  }
  selectDirection(direction, time);
}

void selectDirection(int direction, int time) {
  switch (direction){

     // stop car
     case 0: off(); break;

     // basic directions
     case 1: forward(time); break;
     case 2: reverse(time); break;
     case 3: left(time); break;
     case 4: right(time); break;

     // direction combinations
     case 6: leftForward(time); break;
     case 7: rightForward(time); break;
     case 8: leftReverse(time); break;
     case 9: rightReverse(time); break;

     default: Serial.print("Inalid Command\n");
    }
}

