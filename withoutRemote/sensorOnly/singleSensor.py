import time
import serial
import RPi.GPIO as GPIO

GPIO.setwarnings(False)

def measure():
    # measure distance

  GPIO.output(GPIO_TRIGGER, True)
  time.sleep(0.00001)
  GPIO.output(GPIO_TRIGGER, False)
  start = time.time()

  while GPIO.input(GPIO_ECHO)==0:
    start = time.time()

  while GPIO.input(GPIO_ECHO)==1:
    stop = time.time()

  elapsed = stop-start
  distance = (elapsed * 34300)/2

  return distance

# referring to the pins by GPIO numbers
GPIO.setmode(GPIO.BCM)

# define pi GPIO
GPIO_TRIGGER = 23
GPIO_ECHO    = 24

# output pin: Trigger
GPIO.setup(GPIO_TRIGGER,GPIO.OUT)
# input pin: Echo
GPIO.setup(GPIO_ECHO,GPIO.IN)
# initialize trigger pin to low
GPIO.output(GPIO_TRIGGER, False)

serial_port = serial.Serial('/dev/ttyACM0', 115200, timeout=1)

try:
    while True:
        distance = int(measure())
        print("Distance : %.1f cm" % distance)
        if distance >= 50:
            serial_port.write((chr(1)).encode())
            print("Forward")
        else:
            serial_port.write((chr(0)).encode()) 
            print("Stop")

        # send data to arduino every 0.5 sec
        time.sleep(0.5)
finally:
    GPIO.cleanup()
