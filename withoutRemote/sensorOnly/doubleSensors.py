import time
import serial
import RPi.GPIO as GPIO

GPIO.setwarnings(False)

# referring to the pins by GPIO numbers
GPIO.setmode(GPIO.BCM)

# define pi GPIO
GPIO_FRONT_TRIGGER = 23
GPIO_FRONT_ECHO    = 24
GPIO_BACK_TRIGGER = 27
GPIO_BACK_ECHO    = 22

# output pin: Trigger
GPIO.setup(GPIO_FRONT_TRIGGER,GPIO.OUT)
GPIO.setup(GPIO_BACK_TRIGGER,GPIO.OUT)
# input pin: Echo
GPIO.setup(GPIO_FRONT_ECHO,GPIO.IN)
GPIO.setup(GPIO_BACK_ECHO,GPIO.IN)
# initialize trigger pin to low
GPIO.output(GPIO_FRONT_TRIGGER, False)
GPIO.output(GPIO_BACK_TRIGGER, False)

serial_port = serial.Serial('/dev/ttyACM0', 115200, timeout=1)


# measure front distance
def measureFront():

  GPIO.output(GPIO_FRONT_TRIGGER, True)
  time.sleep(0.00001)
  GPIO.output(GPIO_FRONT_TRIGGER, False)
  frontStart = time.time()

  while GPIO.input(GPIO_FRONT_ECHO)==0:
    frontStart = time.time()

  while GPIO.input(GPIO_FRONT_ECHO)==1:
    frontStop = time.time()

  frontElapsed = frontStop-frontStart
  frontDistance = (frontElapsed * 34300)/2

  return frontDistance


# measure back distance
def measureBack():
  
  GPIO.output(GPIO_BACK_TRIGGER, True)
  time.sleep(0.00001)
  GPIO.output(GPIO_BACK_TRIGGER, False)
  backStart = time.time()

  while GPIO.input(GPIO_BACK_ECHO)==0:
    backStart = time.time()

  while GPIO.input(GPIO_BACK_ECHO)==1:
    backStop = time.time()

  backElapsed = backStop-backStart
  backDistance = (backElapsed * 34300)/2

  return backDistance


try:
    while True:
        frontDistance = int(measureFront())
        print("Front Distance : %.1f cm" % frontDistance)
        backDistance = int(measureBack())
        print("Back Distance : %.1f cm" % backDistance)
        if frontDistance <= 75 and backDistance <=75:
            serial_port.write((chr(0)).encode())
            print("Stop")
        elif  frontDistance <= 75:
            serial_port.write((chr(2)).encode())
            print("Backward")       
        else:
            serial_port.write((chr(1)).encode())
            print("Forward")

        # send data to arduino every 0.5 sec
        time.sleep(0.5)
finally:
    GPIO.cleanup()
