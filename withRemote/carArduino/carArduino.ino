// delare variables for digital pins
int forwardPin = 2;
int backwardPin = 3;
int leftPin = 4;
int rightPin = 5;

// delare variables for output duration
int time = 50;
int direction = 0;

void setup() {
  // initialize the digital pins as an output.
  pinMode(forwardPin, OUTPUT);
  pinMode(backwardPin, OUTPUT);
  pinMode(leftPin, OUTPUT);
  pinMode(rightPin, OUTPUT);
  Serial.begin(115200);
}

void off(){
  digitalWrite(forwardPin, LOW);
  digitalWrite(backwardPin, HIGH);
  digitalWrite(leftPin, LOW);
  digitalWrite(rightPin, LOW);
}

void forward(int time){
  digitalWrite(forwardPin, LOW);
  digitalWrite(backwardPin, LOW);
  //Serial.write("mad");
  delay(time);
}
void backward(int time){
  digitalWrite(backwardPin, HIGH);
  digitalWrite(forwardPin, LOW);
  delay(time);
}
void left(int time){
  digitalWrite(leftPin, HIGH);
  digitalWrite(rightPin, LOW);
  delay(time);
}
void right(int time){
  digitalWrite(rightPin, HIGH);
  digitalWrite(leftPin, LOW);
  delay(time);
}
void leftForward(int time){
  digitalWrite(leftPin, HIGH);
  digitalWrite(forwardPin, HIGH);
  digitalWrite(backwardPin, LOW);
  digitalWrite(rightPin, LOW);
  delay(time);
}
void rightForward(int time){
  digitalWrite(rightPin, HIGH);
  digitalWrite(forwardPin, HIGH);
  digitalWrite(backwardPin, LOW);
  digitalWrite(leftPin, LOW);
  delay(time);
}
void leftBackward(int time){
  digitalWrite(leftPin, HIGH);
  digitalWrite(backwardPin, HIGH);
  digitalWrite(forwardPin, LOW);
  digitalWrite(rightPin, LOW);
  delay(time);
}
void rightBackward(int time){
  digitalWrite(rightPin, HIGH);
  digitalWrite(backwardPin, HIGH);
  digitalWrite(forwardPin, LOW);
  digitalWrite(leftPin, LOW);
  delay(time);
}


void loop() {
  //forward(time);
  //delay(time);
  //backward(time);
  //delay(time);
  //left(time);
  //delay(time);
  //right(time);
  //delay(time);
  //leftBackward(time);
  //delay(time);
  //rightBackward(time);
  //delay(time);
  //leftForward(time);
  //delay(time);
  //rightForward(time);
  //delay(time);
  //off();
  if (Serial.available() > 0){
    direction = Serial.read(); // read command
  }
  else{
    off();
  }
  selectDirection(direction, time);
}

void selectDirection(int direction, int time) {
  switch (direction){

     // stop car
     case 0: off(); break;

     // basic directions
     case 1: forward(time); break;
     case 2: backward(time); break;
     case 3: left(time); break;
     case 4: right(time); break;

     // direction combinations
     case 6: leftForward(time); break;
     case 7: rightForward(time); break;
     case 8: leftBackward(time); break;
     case 9: rightBackward(time); break;

     default: Serial.print("Inalid Command\n");
    }
}


