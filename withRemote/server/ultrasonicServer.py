import socket
import time


class SensorStreamingTest(object):
    def __init__(self):

        self.server_socket = socket.socket()
        self.server_socket.bind(('192.168.1.151', 8000))
        self.server_socket.listen(0)
        self.connection, self.client_address = self.server_socket.accept()
        self.streaming()

    def streaming(self):

        try:
            print "Connection from: ", self.client_address
            start = time.time()

            while True:
                sensor_data = float(self.connection.recv(1024))
                print("Distance: %0.1f cm" % sensor_data)

                # testing for 10 seconds
                if time.time() - start > 10:
                    break
        finally:
            self.connection.close()
            self.server_socket.close()


class RCControl(object):
    def __init__(self):
        self.serial_port = serial.Serial('/dev/tty.usbmodem1421', 115200, timeout=1)
        self.steer()

    def steer(self, prediction):
        if sensor_data <= 4:
            self.serial_port.write(chr(1))
            print("Forward")
        else:
            self.stop()

    def stop(self):
        self.serial_port.write(chr(0))


if __name__ == '__main__':
    SensorStreamingTest()
    RCControl()