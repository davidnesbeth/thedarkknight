import serial
import socket
import time


class SensorStreamingTest(object):
    def __init__(self):

        self.serial_port = serial.Serial('/dev/cu.usbmodem1421', 115200, timeout=1)
        self.server_socket = socket.socket()
        self.server_socket.bind(('192.168.1.151', 8000))
        self.server_socket.listen(0)
        self.connection, self.client_address = self.server_socket.accept()
        self.streaming()

    def streaming(self):

        try:
            print "Connection from: ", self.client_address
            while True:
                sensor_data = int(self.connection.recv(1024))
                if sensor_data >= 75:
                    self.serial_port.write(chr(1))
                    print("Forward")
                else:
                    self.serial_port.write(chr(0)) 
                    print("Stop")
                # print("Distance: %0.1f cm" % sensor_data)

        finally:
            self.connection.close()
            self.server_socket.close()



if __name__ == '__main__':
    SensorStreamingTest()