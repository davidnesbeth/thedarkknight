# Author: David Nesbeth (theDarkNight Autonomous Car)
# Code based on picamera documentation at https://picamera.readthedocs.io/en/release-1.13/recipes1.html#streaming-capture

import io
import socket
import struct
from PIL import Image    # Make sure to install pillow instead of PIL


# Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
# all interfaces)
server_socket = socket.socket()
# Desktop (server) and Raspberry Pi must be on same IP addresss; check ifconfig in Mac terminal
server_socket.bind(('10.36.140.135', 8000))
server_socket.listen(0)

# Accept a single connection and make a file-like object out of it
connection = server_socket.accept()[0].makefile('rb')
count = 0
try:
    while True:
        # Read the length of the image as a 32-bit unsigned int. If the
        # length is zero, quit the loop
        image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
        if not image_len:
            break
        # Construct a stream to hold the image data and read the image
        # data from the connection
        image_stream = io.BytesIO()
        image_stream.write(connection.read(image_len))
        # Rewind the stream, open it as an image with PIL and do some
        # processing on it
        image_stream.seek(0)
        image = Image.open(image_stream)
        print('Image is %dx%d' % image.size)
        # image.verify()
        # print('Image is verified')
        image.save('./images/nez' + str(count) + 'test.png')
        count += 1
finally:
    connection.close()
    server_socket.close()